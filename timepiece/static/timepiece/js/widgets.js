/**
 * File activating any jQuery UI or Bootstrap widgets
 */

$(function() {
    $('[name*=date][name!=week_update],#id_start_time_0').datepicker({
        'dateFormat': 'mm/dd/yy',
	onSelect : function() {
            document.getElementById("id_end_time_0").value = document.getElementById("id_start_time_0").value;
	},
    });

    if( $('[name*=date][name!=week_update],#id_start_time_0').datepicker().val() == "" ) {
	    $('[name*=date][name!=week_update],#id_start_time_0').datepicker().val( getTodaysDate(0) );
    }


    if( $('[name*=date][name!=week_update],#id_end_time_0').datepicker().val() == "" ) {
	    $('[name*=date][name!=week_update],#id_end_time_0').datepicker().val( getTodaysDate(0) );
    }


    $('#id_start_time_1').timepicker({
        onSelect : function(){
            document.getElementById("id_start_time_1").value += ':01';
        },
        hours: {
            starts: 6,                // First displayed hour
            ends: 23                  // Last displayed hour
        },
        defaultTime: '00:00',
        minutes: {
            starts: 0,
            ends:45,
            interval: 15
        },
        rows: 3,                      // Number of rows for the input tables, minimum 2, makes more sense if you use multiple of 2
        showHours: true,              // Define if the hours section is displayed or not. Set to false to get a minute only dialog
        showMinutes: true
    });

    $('#id_end_time_1').timepicker({
        onSelect : function(){
            document.getElementById("id_end_time_1").value += ':00';
        },
        hours: {
            starts: 6,                // First displayed hour
            ends: 23                  // Last displayed hour
        },
        defaultTime: '00:00',
        minutes: {
            starts: 0,
            ends:45,
            interval: 15
        },
        rows: 3,                      // Number of rows for the input tables, minimum 2, makes more sense if you use multiple of 2
        showHours: true,              // Define if the hours section is displayed or not. Set to false to get a minute only dialog
        showMinutes: true
    });



    $('#id_week_start').datepicker({
        'dateFormat': 'yy-mm-dd'
    });

    $('#popover-toggle').popover({
        'title': function() {
            var target = $(this).data('target');

            return $(target).children('.popover-title').html();
        },
        'content': function() {
            var target = $(this).data('target');

            return $(target).children('.popover-content').html();
        },
        'placement': 'bottom',
        'trigger': 'hover'
    });
});

function getTodaysDate (val) {
    var t = new Date, day, month, year = t.getFullYear();
    if (t.getDate() < 10) {
        day = "0" + t.getDate();
    }
    else {
        day = t.getDate();
    }
    if ((t.getMonth() + 1) < 10) {
        month = "0" + (t.getMonth() + 1 - val);
    }
    else {
        month = t.getMonth() + 1 - val;
    }

    return (month + '/' + day + '/' + year);
   }
