jQuery(function($){

function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
var c_value = document.cookie;
var c_start = c_value.indexOf(" " + c_name + "=");
if (c_start == -1)
  {
  c_start = c_value.indexOf(c_name + "=");
  }
if (c_start == -1)
  {
  c_value = null;
  }
else
  {
  c_start = c_value.indexOf("=", c_start) + 1;
  var c_end = c_value.indexOf(";", c_start);
  if (c_end == -1)
  {
c_end = c_value.length;
}
c_value = unescape(c_value.substring(c_start,c_end));
}
return c_value;
}

    $(document).ready(function() {
        form_list = $('input#id_date_form_clear_btn');
        if (form_list.length > 0){
            form_list.click(function() {
                $("ul#ledger-date-fieldlist input[type='text']").each(function() {
                    $(this).val('');
                });
            });
        }
	$('#id_location').change( function () { 
		setCookie("location",$('#id_location').val(), 20 );
	    });

	$('#id_project').change( function () { 
		setCookie("project",$('#id_project').val(), 20 );
	    });

	$('#id_activity').change( function () { 
		setCookie("activity",$('#id_activity').val(), 20 );
	    });


	var loc = getCookie("location");
	if( loc != null ) {
		 $('#id_location').val(loc);
	}
	var prj = getCookie("project");
	if( prj != null ) {
		 $('#id_project').val(prj);
	}
	var prj = getCookie("activity");
	if( prj != null ) {
		 $('#id_activity').val(prj);
	}
    });
});
